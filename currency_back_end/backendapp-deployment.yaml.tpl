apiVersion: apps/v1
kind: Deployment
metadata:
  name: currency-back-end
  labels:
    app: currency-back-end
spec:
  replicas: 1
  selector:
    matchLabels:
      app: currency-back-end
  template:
    metadata:
      labels:
        app: currency-back-end
    spec:
      containers:
        - name: currency-back-end
          image: gcr.io/GOOGLE_CLOUD_PROJECT/IMAGE_NAME:COMMIT_SHA
          ports:
            - containerPort: 8080
          env:
            - name: SPRING_PROFILES_ACTIVE
              value: prod
---
apiVersion: v1
kind: Service
metadata:
  name: currency-back-end
spec:
  selector:
    app: currency-back-end
  ports:
    - protocol: TCP
      port: 80
      targetPort: 8080
  type: ClusterIP