GitOps-style continuous delivery
-----------------------------------
I setup a CI/CD pipeline on Google Cloud using a GitOps approach.

This enables describing deployments declaratively using manifests files stored in a Git repository.

The CI pipeline trigger was created in GCP Cloudbuild that builds each application image when pushes are made to separate branches in the repository that I
hosted on BitBucket

Built images were pushed to Google Container Registry.

The final step in the CI is the update of a deployement file with the currently built image hash and subsequent push of that file
to the currency-app-env repository setup on SourceRepo on GCP.

A CD trigger was created on the currency-app-env repo's candidate (for the backend app) and candidate-frontend branches.

Commits to this repo immediately builds and deploys the application into the GKE cluster.

Afterwards a copy of the manifest is pushed to the production branch to keep a history of all deployment manifests.

The frontend application was exposed using a NodePort service to prevent incurring further costs associated with provisioning
a LoadBalancer service on GCP.

Configuration
-----------------
The backend application configuration were kept in application-prod.properties and the SPRING_PROFILES_ACTIVE was passed as an environment variable to the backend application.

The frontend application configuration that contained the backend application URL were created in the /environments/environment.prod.ts file
the production configuration was passed to the docker build command to replace the environment file.

The environment file object is referenced in the services to obtain the backend application URL instead of hardcoding it.

Other things like OktaAuthService clientIds were left in the code as there was no way secure way other than environment files to pass these values to the
application at build time. Secrets and environment variables could not be used because these would not available in the browser.

A more secure approach would be to load these from the backend or have the backend handle authentication with Okta and not expose the token to the frontend.


Scalable Architecture
-----------------------
One way to handle traffic is to configure Horizontal Auto-scaling with kubernetes based on different metrics such as CPU or memory usages.

A maximum number of replicas would be configured and the platform would be allowed to handle this.

To improve response times, caching could also be leveraged both at Nginx layer as well as the backend application.


Issues Faced
---------------
Due to the fact that the application was hosted without TLS the frontend throws the an error when the History, Currency and Saved tabs are clicked.
On researching the issue, I came across this issues link.