import {ConfigVariables} from './config-variables'

export const environment = {
  production: true,
  backendUrl: 'http://currency-back-end.svc.cluster.local:8080'
};
