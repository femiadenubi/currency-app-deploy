apiVersion: apps/v1
kind: Deployment
metadata:
  name: currency-front-end
  labels:
    app: currency-front-end
spec:
  replicas: 1
  selector:
    matchLabels:
      app: currency-front-end
  template:
    metadata:
      labels:
        app: currency-front-end
    spec:
      containers:
        - name: currency-front-end
          image: gcr.io/GOOGLE_CLOUD_PROJECT/IMAGE_NAME:COMMIT_SHA
          ports:
            - containerPort: 80
          env:
            - name: SPRING_PROFILES_ACTIVE
              value: prod
---
apiVersion: v1
kind: Service
metadata:
  name: currency-front-end
spec:
  selector:
    app: currency-front-end
  ports:
    - protocol: TCP
      nodePort: 30080
      port: 80
      targetPort: 80
  type: NodePort
